﻿namespace PatientService.Test
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using PatientService.Common;
    using PatientService.Core.AuthenticationManager;
    using PatientService.Core.AuthenticationManager.Models;
    using PatientService.Core.UserManager;
    using PatientService.Core.UserManager.Models;
    using PatientService.Database;
    using PatientService.Database.Models;
    using PatientService.Database.Repositories;

    [TestClass]
    public class AuthenticationTest
    {

        [TestMethod]
        public async Task TestIsAuthenticationRequestValidTrue()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureCreated();
                var userRepository = new RepositoryBase<User, PatientDataContext>(context);
                var authenticationValidator = new AuthenticationValidator(userRepository);


                var authenticationRequest = new AuthenticationRequestModel
                {
                    Email = "test@test.com",
                    Password = "test2023@"
                };

                //Act
                var result = await authenticationValidator.IsAuthenticationRequestValidAsync(authenticationRequest);

                //Assert
                Assert.IsTrue(result.IsValid);
            }
        }

        [TestMethod]
        public async Task TestIsAuthenticationRequestValidFalse()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureCreated();
                var userRepository = new RepositoryBase<User, PatientDataContext>(context);
                var authenticationValidator = new AuthenticationValidator(userRepository);


                var authenticationRequest = new AuthenticationRequestModel
                {
                    Email = "",
                    Password = "password"
                };

                //Act
                var result = await authenticationValidator.IsAuthenticationRequestValidAsync(authenticationRequest);

                //Assert
                Assert.IsFalse(result.IsValid);
            }
        }

        [TestMethod]
        public async Task TestIsPasswordValidTrue()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureCreated();
                var userRepository = new RepositoryBase<User, PatientDataContext>(context);
                var authenticationValidator = new AuthenticationValidator(userRepository);
                var userManager = new UserManager(userRepository, new UserValidator());

                await userManager.CreateUserAsync(new CreateUserRequestModel
                {
                    Email = "test@domain.com",
                    Password = "Pass2023"
                });

                var user = await context.Users.FirstOrDefaultAsync(u => u.Email.Equals("test@domain.com"));

                //Act
                var result = authenticationValidator.IsPasswordValid("Pass2023", user!.PasswordHash);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task TestIsPasswordValidFalse()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureCreated();
                var userRepository = new RepositoryBase<User, PatientDataContext>(context);
                var authenticationValidator = new AuthenticationValidator(userRepository);
                var userManager = new UserManager(userRepository, new UserValidator());

                await userManager.CreateUserAsync(new CreateUserRequestModel
                {
                    Email = "test@domain.com",
                    Password = "Pass2023"
                });

                var user = await context.Users.FirstOrDefaultAsync(u => u.Email.Equals("test@domain.com"));

                //Act
                var result = authenticationValidator.IsPasswordValid("Pass2022", user!.PasswordHash);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task TestAuthenticateUser()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                IOptions<AppSettings> settingsOptions = Options.Create(new AppSettings 
                { 
                    Audience = "test.com/api",
                    Issuer = "www.test.com",
                    Key = "testkeyJHNiug-234.sdasdasda3"
                });

                var userRepository = new RepositoryBase<User, PatientDataContext>(context);
                var authenticationValidator = new AuthenticationValidator(userRepository);
                var authenticationManager = new AuthenticationManager(userRepository,
                                                                      authenticationValidator,
                                                                      settingsOptions);

                //Act
                var authResult = await authenticationManager.AuthenticateAsync(new AuthenticationRequestModel 
                {
                    Email = "test@test.com", 
                    Password = "test2023@" 
                });

                string token = null;
                
                authResult.IfSucc(value =>
                {
                    token = value.Token;
                });

                //Assert
                Assert.IsNotNull(token);
            }
        }
    }
}