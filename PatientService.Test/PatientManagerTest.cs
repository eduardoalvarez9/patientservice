﻿using Microsoft.EntityFrameworkCore;
using PatientService.Database.Models;
using PatientService.Database.Repositories;
using PatientService.Database;
using PatientService.Core.PatientManager;
using PatientService.Core.PatientManager.Validators;
using PatientService.Core.PatientManager.Models.CreatePatient;
using System.ComponentModel.DataAnnotations;
using PatientService.Core.PatientManager.Models.UpdatePatient;

namespace PatientService.Test
{

    [TestClass]
    public class PatientManagerTest
    {
        [TestMethod]
        public async Task CreatePatientWithoutAddress()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                
                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                CreatePatientResponseModel patientCreate = null;
                var result = await patientManager.CreateAsync(new CreatePatientRequestModel 
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 23444235,
                    MobilePhone = "+504 94805277"
                });

                result.IfSucc(response =>
                {
                    patientCreate = response;
                });

                //Assert
                Assert.IsNotNull(patientCreate);
            }
        }

        [TestMethod]
        public async Task CreatePatientWithAddress()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                CreatePatientResponseModel patientCreate = null;
                var result = await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 23444235,
                    MobilePhone = "+504 94805277",
                    Addresses = new List<CreatePatientAddressRequestModel> {
                        new CreatePatientAddressRequestModel
                        {
                            City = "City",
                            FirstLine = "street",
                            SecondLine = "2ds",
                            Country = "Canada",
                            ZipCode = "15465",
                            Details = "blue door"
                        }
                    }
                });

                result.IfSucc(response =>
                {
                    patientCreate = response;
                });

                //Assert
                Assert.IsNotNull(patientCreate.Addresses);
            }
        }

        [TestMethod]
        public async Task CreatePatientValidationMessage()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                ValidationException validation = null;
                 await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 1,
                    MobilePhone = "+504 94805277"
                });

                var result = await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 1,
                    MobilePhone = "+504 94805277"
                });

                result.IfFail(response =>
                {
                    if(response is ValidationException exception)
                    validation = exception;
                });

                //Assert
                Assert.AreEqual(validation.Message, "Insurance number exist");
            }
        }

        [TestMethod]
        public async Task DeletePatient()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                
                await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 23444235,
                    MobilePhone = "+504 94805277",
                });

                var result = await  patientManager.DeleteAsync(1);
                bool patientDeleted = false;
                result.IfSucc(response =>
                {
                    patientDeleted = response;
                });

                //Assert
                Assert.IsTrue(patientDeleted);
            }
        }

        [TestMethod]
        public async Task DeletePatientNotFound()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act


                await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 23444235,
                    MobilePhone = "+504 94805277",
                });

                await patientManager.DeleteAsync(1);
                var result = await patientManager.DeleteAsync(1);
                bool patientDeleted = true;
                result.IfSucc(response =>
                {
                    patientDeleted = response;
                });

                //Assert
                Assert.IsFalse(patientDeleted);
            }
        }

        [TestMethod]
        public async Task UpdatePatient()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                 await patientManager.CreateAsync(new CreatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 1,
                    MobilePhone = "+504 94805277"
                });
                
                var result = await patientManager.UpdateAsync(new UpdatePatientRequestModel
                {
                    FirstName = "Updated Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 1,
                    MobilePhone = "+504 94805277"
                }, 1);

                string updatedName = "";
                result.IfSucc(response =>
                {
                    updatedName = response.FirstName;
                });

                //Assert
                Assert.AreEqual(updatedName, "Updated Name");
            }
        }

        [TestMethod]
        public async Task UpdatePatientNotFoundValidation()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<PatientDataContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;

            using (var context = new PatientDataContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var patientRepository = new RepositoryBase<Patient, PatientDataContext>(context);
                var patientValidator = new PatientValidator(patientRepository);
                var patientManager = new PatientManager(patientRepository, patientValidator);
                //Act

                ValidationException validation = null;

                var result = await patientManager.UpdateAsync(new UpdatePatientRequestModel
                {
                    FirstName = "Test Name",
                    LastName = "LastName Test",
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    Email = "email@domain.com",
                    Height = 5.6,
                    Weight = 70.5,
                    InsuranceNumber = 1,
                    MobilePhone = "+504 94805277"
                },4);

                result.IfFail(response =>
                {
                    if (response is ValidationException exception)
                        validation = exception;
                });

                //Assert
                Assert.AreEqual(validation.Message, "User not found");
            }
        }
    }
}
