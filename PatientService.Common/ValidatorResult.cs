﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Common
{
    public class ValidatorResult
    {
        public ValidatorResult()
        {
        }

        public ValidatorResult(string errorMessage)
        {
            this.ErrorMessage = errorMessage;
        }

        public bool IsValid { get  
            {
                return string.IsNullOrEmpty(this.ErrorMessage);
            }}

        public string ErrorMessage { get; set; }
    }
}
