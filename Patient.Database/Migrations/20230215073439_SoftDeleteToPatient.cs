﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PatientService.Database.Migrations
{
    public partial class SoftDeleteToPatient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Patients",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "PasswordHash",
                value: "$2a$11$NIzZVEg8xjpMj989Q8CRiuWAraKgj.VwKpdUbbR0/n.Mgfr4vrSHm");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Patients");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "PasswordHash",
                value: "$2a$11$uuUY6n/F8WF1hyWovuuILOrJdAIqdDNQnWWqhVtCvyl6yJwAs.4VC");
        }
    }
}
