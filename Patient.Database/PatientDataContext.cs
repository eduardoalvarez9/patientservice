﻿namespace PatientService.Database
{
    using Microsoft.EntityFrameworkCore;
    using PatientService.Database.Models;
    using BCrypt.Net;
    public class PatientDataContext : DbContext
    {
        public PatientDataContext(DbContextOptions<PatientDataContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientAddress> PatientAddresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>().HasQueryFilter(s => !s.Deleted);

            modelBuilder.Entity<PatientAddress>(o =>
            {
                o.HasOne(p => p.Patient)
                .WithMany(p => p.Addresses);
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 1,
                Email = "test@test.com",
                PasswordHash = BCrypt.HashPassword("test2023@")
            });
        }
    }
}