﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Database.Models
{
    public class Patient
    {
        public int Id { get; set; }
        public int InsuranceNumber { get; set; }
        public string FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public string MobilePhone { get; set; }
        public bool Deleted { get; set; }
        public ICollection<PatientAddress> Addresses { get; set; }
    }
}
