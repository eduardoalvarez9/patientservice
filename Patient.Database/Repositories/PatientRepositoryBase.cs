﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Database.Repositories
{
    public class PatientRepositoryBase<TEntity> : RepositoryBase<TEntity, PatientDataContext>
       where TEntity : class
    {
        public PatientRepositoryBase(PatientDataContext context)
            : base(context)
        {
        }
    }
}
