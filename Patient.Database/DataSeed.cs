﻿namespace PatientService.Database
{
    using Microsoft.EntityFrameworkCore;
    using PatientService.Database.Models;
    using BCrypt.Net;
    using PatientService.Database.Repositories;

    public class DataSeed
    {
        private readonly IRepository<Patient> _patientRepository;
        private readonly IRepository<PatientAddress> _addressRepository;

        public DataSeed(IRepository<Patient> patientRepository,
            IRepository<PatientAddress> addressRepository)
        {
            _patientRepository = patientRepository;
            _addressRepository = addressRepository;
        }

        public async Task SeedPatient()
        {
            var patient = await _patientRepository.FirstOrDefaultAsync(s => s.FirstName != null);
            if(patient == null)
            {
                var patients = new List<Patient>();
                for (int i = 0; i < 1000000; i++)
                {
                    patients.Add(new Patient
                    {
                        FirstName = $"patient {i}",
                        LastName = $"lastName {i}",
                        DateOfBirth = DateTime.Now.AddMinutes(-i),
                        Email = $"mail{i}@domain.com",
                        InsuranceNumber = i + 1,
                        MobilePhone = $"+1 548 {i}",
                        Height = 5.6,
                        Weight = 60.4,
                    });
                }
                await _patientRepository.CreateBulkAsync(patients);

                var addresses = new List<PatientAddress>();

                for (int i = 0; i < 1000000; i++)
                {
                    addresses.Add(new PatientAddress
                    {
                        PatientId = i + 1,
                        City = $"City{i}",
                        FirstLine = $"street {i}",
                        Country = "Canada",
                        ZipCode = $"{i}",
                    });
                }
                await _addressRepository.CreateBulkAsync(addresses);
                
            }
        }
    }
}
