﻿using LanguageExt.Common;
using PatientService.Core.UserManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.UserManager
{
    public interface IUserManager
    {
        Task<Result<bool>> CreateUserAsync(CreateUserRequestModel user);
    }
}
