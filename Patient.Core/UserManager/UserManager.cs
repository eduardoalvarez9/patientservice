﻿namespace PatientService.Core.UserManager
{
    using LanguageExt.Common;
    using PatientService.Core.UserManager.Models;
    using PatientService.Database;
    using PatientService.Database.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BCrypt.Net;
    using System.ComponentModel.DataAnnotations;
    using PatientService.Database.Repositories;

    public class UserManager : IUserManager
    {
        private readonly IRepository<User> _userRepository;
        private readonly UserValidator _userValidator;

        public UserManager(IRepository<User> userRepository,
            UserValidator userValidator)
        {
            _userRepository = userRepository;
            _userValidator = userValidator;
        }

        public async Task<Result<bool>> CreateUserAsync(CreateUserRequestModel user)
        {
            try 
            {
                var validatorResult = _userValidator.IsCreateUserRequestValid(user);
                if (!validatorResult.IsValid)
                {
                    return new Result<bool>(new ValidationException(validatorResult.ErrorMessage));
                }

                var newUser = new User
                {
                    Email = user.Email,
                    PasswordHash = BCrypt.HashPassword(user.Password)
                };

                _userRepository.Create(newUser);
                await _userRepository.SaveChangesAsync();

                return new Result<bool>(true);
            }
            catch (Exception ex)
            {
                return new Result<bool>(ex);
            }
        }
    }
}
