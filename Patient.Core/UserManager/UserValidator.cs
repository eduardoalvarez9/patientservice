﻿using PatientService.Common;
using PatientService.Core.AuthenticationManager.Models;
using PatientService.Core.UserManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.UserManager
{
    public class UserValidator
    {
        public ValidatorResult IsCreateUserRequestValid(CreateUserRequestModel requestModel)
        {
            if (string.IsNullOrEmpty(requestModel.Email) ||
                string.IsNullOrEmpty(requestModel.Password))
            {
                return new ValidatorResult("User and password required");
            }
            return new ValidatorResult();
        }
    }
}
