﻿using LanguageExt.Common;
using PatientService.Core.AuthenticationManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.AuthenticationManager
{
    public interface IAuthenticationManager
    {
        Task<Result<AuthenticationResponseModel>> AuthenticateAsync(AuthenticationRequestModel request);
    }
}
