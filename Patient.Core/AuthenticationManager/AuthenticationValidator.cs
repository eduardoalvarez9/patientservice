﻿namespace PatientService.Core.AuthenticationManager    
{
    using PatientService.Core.AuthenticationManager.Models;
    using BCrypt.Net;
    using PatientService.Common;
    using PatientService.Database.Repositories;
    using PatientService.Database.Models;
    using LanguageExt.Common;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.IdentityModel.Tokens;

    public class AuthenticationValidator
    {
        private readonly IRepository<User> _userRepository;
        public AuthenticationValidator(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ValidatorResult> IsAuthenticationRequestValidAsync(AuthenticationRequestModel requestModel)
        {
            if (string.IsNullOrEmpty(requestModel.Email) ||
                string.IsNullOrEmpty(requestModel.Password))
            {
                return new ValidatorResult("Incorrect email or password.");
            }

            var user = await _userRepository.FirstOrDefaultAsync(u => u.Email.Equals(requestModel.Email));

            if (user == null)
            {
                return new ValidatorResult("User not found.");
            }

            if (!this.IsPasswordValid(requestModel.Password, user.PasswordHash))
            {
                return new ValidatorResult("Incorrect email or password.");
            }

            return new ValidatorResult();
        }

        public bool IsPasswordValid(string password, string hashPassword)
        {
            return BCrypt.Verify(password, hashPassword);
        }
    }
}
