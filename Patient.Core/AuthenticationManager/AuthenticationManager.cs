﻿using LanguageExt.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PatientService.Common;
using PatientService.Core.AuthenticationManager.Models;
using PatientService.Database;
using PatientService.Database.Models;
using PatientService.Database.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.AuthenticationManager
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private readonly IRepository<User> _userRepository;
        private readonly AuthenticationValidator _validator;
        private readonly AppSettings _appSettings;
        public AuthenticationManager(
            IRepository<User> userRepository, 
            AuthenticationValidator validator,
            IOptions<AppSettings> appSettingsOptions)
        {
            _userRepository = userRepository;
            _validator = validator;
            _appSettings = appSettingsOptions.Value;
        }

        public async Task<Result<AuthenticationResponseModel>> AuthenticateAsync(AuthenticationRequestModel request)
        {
            try
            {
                var validationResult = await _validator.IsAuthenticationRequestValidAsync(request);
                if (!validationResult.IsValid)
                {
                    return new Result<AuthenticationResponseModel>(new ValidationException(validationResult.ErrorMessage));
                }

                var user = await _userRepository.FirstOrDefaultAsync(u => u.Email.Contains(request.Email));

                return new Result<AuthenticationResponseModel>(new AuthenticationResponseModel
                {
                    Token = this.GenerateToken(user)
                }); 
            } 
            catch (Exception ex)
            {
                return new Result<AuthenticationResponseModel>(ex);
            }
        }

        private string GenerateToken(User user)
        {
            var symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_appSettings.Key)
                );
            var signingCredentials = new SigningCredentials(
                    symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var header = new JwtHeader(signingCredentials);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };

            var payload = new JwtPayload(
                  issuer: _appSettings.Issuer,
                  audience: _appSettings.Audience,
                  claims: claims,
                  notBefore: DateTime.UtcNow,
                  expires: DateTime.UtcNow.AddHours(24)
              );

            var token = new JwtSecurityToken(
                    header,
                    payload
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
