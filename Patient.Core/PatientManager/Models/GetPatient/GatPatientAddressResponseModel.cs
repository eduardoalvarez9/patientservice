﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.PatientManager.Models.GetPatient
{
    public class GatPatientAddressResponseModel
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string FirstLine { get; set; }
        public string? SecondLine { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string? Details { get; set; }
    }
}
