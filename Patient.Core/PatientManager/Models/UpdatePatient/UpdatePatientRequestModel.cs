﻿using PatientService.Core.PatientManager.Models.CreatePatient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.PatientManager.Models.UpdatePatient
{
    public class UpdatePatientRequestModel
    {
        [Required(ErrorMessage = "Field InsuranceNumber is required")]
        public int InsuranceNumber { get; set; }

        [Required(ErrorMessage = "Field FirstName is required")]
        public string FirstName { get; set; }

        public string? MiddleName { get; set; }

        [Required(ErrorMessage = "Field LastName is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Field DateOfBirth is required")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Field Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Field Weight is required")]
        public double Weight { get; set; }

        [Required(ErrorMessage = "Field Height is required")]
        public double Height { get; set; }

        public string MobilePhone { get; set; }
    }
}
