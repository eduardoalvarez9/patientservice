﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.PatientManager.Models.UpdatePatient
{
    public class UpdatePatientResponseModel
    {
        public int Id { get; set; }
        public int InsuranceNumber { get; set; }
        public string FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public string MobilePhone { get; set; }
    }
}
