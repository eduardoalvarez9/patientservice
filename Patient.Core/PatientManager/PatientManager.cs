﻿using LanguageExt.Common;
using LanguageExt.Pretty;
using Microsoft.EntityFrameworkCore;
using PatientService.Common;
using PatientService.Core.PatientManager.Models.CreatePatient;
using PatientService.Core.PatientManager.Models.GetPatient;
using PatientService.Core.PatientManager.Models.UpdatePatient;
using PatientService.Core.PatientManager.Validators;
using PatientService.Database.Models;
using PatientService.Database.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.PatientManager
{
    public class PatientManager : IPatientManager
    {
        private readonly IRepository<Patient> _patientRepository;
        private readonly PatientValidator _patientValidator;

        public PatientManager(IRepository<Patient> patientRepository,
            PatientValidator patientValidator)
        {
            _patientRepository = patientRepository;
            _patientValidator = patientValidator;
        }

        public async Task<Result<CreatePatientResponseModel>> CreateAsync(CreatePatientRequestModel request)
        {
            try 
            {
                var validationResult = await _patientValidator.CanCreatePatientAsync(request);
                if (!validationResult.IsValid)
                {
                    ValidationException validationException = new ValidationException(validationResult.ErrorMessage);
                    return new Result<CreatePatientResponseModel>(validationException);
                }

                var newPatient = this.MapPatient(request);
                var result = _patientRepository.Create(newPatient);
                await _patientRepository.SaveChangesAsync();

                return new Result<CreatePatientResponseModel>(new CreatePatientResponseModel
                {
                    Id = result.Id,
                    FirstName = result.FirstName,
                    MiddleName = result.MiddleName,
                    LastName = result.LastName,
                    InsuranceNumber = result.InsuranceNumber,
                    DateOfBirth = result.DateOfBirth,
                    Email = result.Email,
                    Height = result.Height,
                    Weight = result.Weight,
                    MobilePhone = result.MobilePhone,
                    Addresses = result.Addresses.Select(s => new CreatePatientAddressResponseModel
                    {
                        PatientId = s.Id,
                        FirstLine = s.FirstLine,
                        SecondLine = s.SecondLine,
                        City = s.City,
                        Country = s.Country,
                        ZipCode = s.ZipCode,
                        Details = s.Details,
                    }).ToList()
                });
            }
            catch (Exception ex) 
            {
                return new Result<CreatePatientResponseModel>(ex);
            }
        }

        public async Task<Result<GetPatientResponseModel>> GetByIdAsync(int id)
        {
            try
            {
                var result = await _patientRepository.All()
                    .Include(s => s.Addresses)
                    .FirstOrDefaultAsync(s => s.Id.Equals(id));
                
                if (result == null)
                {
                    return null;
                }

                return new GetPatientResponseModel
                {
                    Id = result.Id,
                    FirstName = result.FirstName,
                    MiddleName = result.MiddleName,
                    LastName = result.LastName,
                    InsuranceNumber = result.InsuranceNumber,
                    DateOfBirth = result.DateOfBirth,
                    Email = result.Email,
                    MobilePhone = result.MobilePhone,
                    Height = result.Height,
                    Weight = result.Weight,
                    Addresses = result.Addresses.Select(s => new GatPatientAddressResponseModel
                    {
                        PatientId = s.Id,
                        FirstLine = s.FirstLine,
                        SecondLine = s.SecondLine,
                        City = s.City,
                        Country = s.Country,
                        ZipCode = s.ZipCode,
                        Details = s.Details,
                    }).ToList()
                };
            }
            catch (Exception ex)
            {
                return new Result<GetPatientResponseModel>(ex);
            }
        }

        public async Task<Result<GetPatientResponseModel>> GetByInsuranceNumberAsync(int insuranceNumber)
        {
            try
            {
                var result = await _patientRepository.All()
                    .Include(s => s.Addresses)
                    .FirstOrDefaultAsync(s => s.InsuranceNumber.Equals(insuranceNumber));

                if (result == null)
                {
                    return null;
                }

                return new GetPatientResponseModel
                {
                    Id = result.Id,
                    FirstName = result.FirstName,
                    MiddleName = result.MiddleName,
                    LastName = result.LastName,
                    InsuranceNumber = result.InsuranceNumber,
                    DateOfBirth = result.DateOfBirth,
                    Email = result.Email,
                    MobilePhone = result.MobilePhone,
                    Height = result.Height,
                    Weight = result.Weight,
                    Addresses = result.Addresses.Select(s => new GatPatientAddressResponseModel
                    {
                        PatientId = s.Id,
                        FirstLine = s.FirstLine,
                        SecondLine = s.SecondLine,
                        City = s.City,
                        Country = s.Country,
                        ZipCode = s.ZipCode,
                        Details = s.Details,
                    }).ToList()
                };
            }
            catch (Exception ex)
            {
                return new Result<GetPatientResponseModel>(ex);
            }
        }

        public Result<PagedList<Patient>> GetPatients(QueryStringPaginationParameters paginationParams)
        {
            try
            {
                return PagedList<Patient>.ToPagedList(_patientRepository.All().OrderBy(o => o.Id).Include(s => s.Addresses),
                                                paginationParams.PageNumber,
                                                paginationParams.PageSize);
            }
            catch(Exception ex)
            {
                return new Result<PagedList<Patient>>(ex);
            }
        }

        public async Task<Result<bool>> DeleteAsync(int id)
        {
            try
            {
                var patient = await _patientRepository.FirstOrDefaultAsync(s => s.Id.Equals(id));
                if(patient != null)
                {
                    patient.Deleted = true;
                    _patientRepository.Update(patient);
                    await _patientRepository.SaveChangesAsync();
                    return new Result<bool>(true);
                }
                return new Result<bool>(false);
            }
            catch (Exception ex)
            {
                return new Result<bool>(ex);
            }
        }

        public async Task<Result<UpdatePatientResponseModel>> UpdateAsync(UpdatePatientRequestModel request, int id)
        {

            try
            {
                var validationResult = await _patientValidator.CanCreateUpdateAsync(request, id);
                if (!validationResult.IsValid)
                {
                    ValidationException validationException = new ValidationException(validationResult.ErrorMessage);
                    return new Result<UpdatePatientResponseModel>(validationException);
                }

                var patient = _patientRepository.FirstOrDefault(s => s.Id.Equals(id));
               
                patient.FirstName = request.FirstName;
                patient.MiddleName = request.MiddleName;
                patient.LastName = request.LastName;
                patient.InsuranceNumber = request.InsuranceNumber;
                patient.DateOfBirth = request.DateOfBirth;
                patient.Email = request.Email;
                patient.MobilePhone = request.MobilePhone;
                patient.Height = request.Height;
                patient.Weight = request.Weight;

                var result = _patientRepository.Update(patient);
                await _patientRepository.SaveChangesAsync();

                return new Result<UpdatePatientResponseModel>(new UpdatePatientResponseModel
                {
                    Id = result.Id,
                    FirstName = result.FirstName,
                    MiddleName = result.MiddleName,
                    LastName = result.LastName,
                    InsuranceNumber = result.InsuranceNumber,
                    DateOfBirth = result.DateOfBirth,
                    Email = result.Email,
                    Height = result.Height,
                    Weight = result.Weight,
                    MobilePhone = result.MobilePhone
                });
            }
            catch (Exception ex)
            {
                return new Result<UpdatePatientResponseModel>(ex);
            }
        }

        private Patient MapPatient(CreatePatientRequestModel request)
        {
            var addresses = new List<PatientAddress>();
            if (request.Addresses != null)
            {
                addresses = request.Addresses.Select(s => new PatientAddress
                {
                    FirstLine = s.FirstLine,
                    SecondLine = s.SecondLine,
                    City = s.City,
                    Country = s.Country,
                    ZipCode = s.ZipCode,
                    Details = s.Details,
                }).ToList();
            }

            return new Patient
            {
                FirstName = request.FirstName,
                MiddleName = request.MiddleName,
                LastName = request.LastName,
                InsuranceNumber = request.InsuranceNumber,
                DateOfBirth = request.DateOfBirth,
                Email = request.Email,
                MobilePhone = request.MobilePhone,
                Height = request.Height,
                Weight = request.Weight,
                Addresses = addresses
            };
        }
    }
}
