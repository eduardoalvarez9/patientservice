﻿using LanguageExt.Common;
using PatientService.Common;
using PatientService.Core.PatientManager.Models.CreatePatient;
using PatientService.Core.PatientManager.Models.GetPatient;
using PatientService.Core.PatientManager.Models.UpdatePatient;
using PatientService.Database.Models;

namespace PatientService.Core.PatientManager
{
    public interface IPatientManager
    {
        Task<Result<CreatePatientResponseModel>> CreateAsync(CreatePatientRequestModel request);
        Task<Result<UpdatePatientResponseModel>> UpdateAsync(UpdatePatientRequestModel request, int id);
        Result<PagedList<Patient>> GetPatients(QueryStringPaginationParameters paginationParams);
        Task<Result<GetPatientResponseModel>> GetByIdAsync(int id);
        Task<Result<GetPatientResponseModel>> GetByInsuranceNumberAsync(int insuranceNumber);
        Task<Result<bool>> DeleteAsync(int id);
    }
}
