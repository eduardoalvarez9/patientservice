﻿using PatientService.Common;
using PatientService.Core.PatientManager.Models.CreatePatient;
using PatientService.Core.PatientManager.Models.UpdatePatient;
using PatientService.Database.Models;
using PatientService.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientService.Core.PatientManager.Validators
{
    public class PatientValidator
    {
        private readonly IRepository<Patient> _patientRepository;
        public PatientValidator(IRepository<Patient> patientRepository)
        {
            _patientRepository = patientRepository;
        }

        public async Task<ValidatorResult> CanCreatePatientAsync(CreatePatientRequestModel requestModel) 
        {
            var validator = new ValidatorResult();

            var insuranceNumberExist = await _patientRepository.FirstOrDefaultAsync(s => s.InsuranceNumber.Equals(requestModel.InsuranceNumber));
            if(insuranceNumberExist != null)
            {
                validator.ErrorMessage =  "Insurance number exist";
                return validator;
            }

            return validator;
        }

        public async Task<ValidatorResult> CanCreateUpdateAsync(UpdatePatientRequestModel requestModel, int id)
        {
            var validator = new ValidatorResult();

            var userExist = await _patientRepository.FirstOrDefaultAsync(s => s.Id.Equals(id));
            if (userExist == null)
            {
                validator.ErrorMessage = "User not found";
                return validator;
            }

            return validator;
        }
    }
}
