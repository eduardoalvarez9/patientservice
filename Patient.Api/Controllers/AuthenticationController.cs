﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PatientService.Core.AuthenticationManager;
using PatientService.Core.AuthenticationManager.Models;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace PatientService.Api.Controllers
{
    [Route("api/authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationManager _authenticationManager;
        public AuthenticationController(IAuthenticationManager authenticationManager)
        {
            _authenticationManager = authenticationManager;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync(AuthenticationRequestModel request)
        {
            var result = await _authenticationManager.AuthenticateAsync(request);

            return result.Match<IActionResult>(value =>
            {
                return this.Ok(value);
            }, exception =>
            {
                if(exception is ValidationException validationException)
                {
                    return BadRequest(validationException.Message);
                }
                return StatusCode(500, "Something went wrong");
            });
        }
    }
}
