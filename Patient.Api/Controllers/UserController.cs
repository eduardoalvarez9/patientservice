﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PatientService.Core.UserManager;
using PatientService.Core.UserManager.Models;

namespace PatientService.Api.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateUserRequestModel userRequest)
        {
            var result = await _userManager.CreateUserAsync(userRequest);
            
            return result.Match<IActionResult>(response =>
            {
                return StatusCode(201);
            }, exception =>
            {
                var message = exception.Message;
                return BadRequest();
            });
        }
    }
}
