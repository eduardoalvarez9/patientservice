﻿using Azure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PatientService.Common;
using PatientService.Core.PatientManager;
using PatientService.Core.PatientManager.Models.CreatePatient;
using PatientService.Core.PatientManager.Models.GetPatient;
using PatientService.Core.PatientManager.Models.UpdatePatient;
using PatientService.Database.Models;
using System.ComponentModel.DataAnnotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PatientService.Api.Controllers
{
    [Route("api/patient")]
    [Authorize]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientManager _patientManager;

        public PatientController(IPatientManager patientManager)
        {
            _patientManager = patientManager;
        }

        [HttpGet("get")]
        public IActionResult GatPatients([FromQuery] QueryStringPaginationParameters paginationParameters)
        {
            var result = _patientManager.GetPatients(paginationParameters);

            return result.Match<IActionResult>(response => 
            {
              
                var patients = this.MapGetPatientResponse(response);
                return Ok(patients);
            }, 
            exeption =>
            {
                return StatusCode(500);
            });
        }

        [HttpGet("getById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _patientManager.GetByIdAsync(id);

            return result.Match<IActionResult>(response =>
            {
                if(response == null)
                {
                    return NotFound();
                }

                return Ok(response);
            },
            exeption =>
            {
                return StatusCode(500);
            });
        }

        [HttpGet("getByInsuranceNumber/{insuraneNumber}")]
        public async Task<IActionResult> GetByInsuranceNumber(int insuraneNumber)
        {
            var result = await _patientManager.GetByInsuranceNumberAsync(insuraneNumber);

            return result.Match<IActionResult>(response =>
            {
                if (response == null)
                {
                    return NotFound();
                }
                return Ok(response);
            },
            exeption =>
            {
                return StatusCode(500);
            });
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreatePatientRequestModel request)
        {
            var result = await _patientManager.CreateAsync(request);

            return result.Match<IActionResult>(response =>
            {
                return Ok(response);
            }, exception =>
            {
                if(exception is ValidationException validation)
                {
                    return BadRequest(validation.Message);
                }
                return StatusCode(500);
            });
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> CreateAsync([FromBody] UpdatePatientRequestModel request, int id)
        {
            var result = await _patientManager.UpdateAsync(request, id);

            return result.Match<IActionResult>(response =>
            {
                return Ok(response);
            }, exception =>
            {
                if (exception is ValidationException validation)
                {
                    return BadRequest(validation.Message);
                }
                return StatusCode(500);
            });
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _patientManager.DeleteAsync(id);

            return result.Match<IActionResult>(response =>
            {
                if (response)
                {
                    return this.Ok(response);
                }
                return NotFound("Patient not found");
            }, exception =>
            {
                return StatusCode(500);
            });
        }

        private PagedResponse<PagedList<GetPatientResponseModel>> MapGetPatientResponse(PagedList<Patient> patients)
        {
            var patientResponse = patients.Select(s => new GetPatientResponseModel
            {
                Id = s.Id,
                InsuranceNumber = s.InsuranceNumber,
                FirstName = s.FirstName,
                MiddleName = s.MiddleName,
                LastName = s.LastName,
                DateOfBirth = s.DateOfBirth,
                Email = s.Email,
                Height = s.Height,
                Weight = s.Weight,
                MobilePhone = s.MobilePhone,
                Addresses = s.Addresses.Select(a => new GatPatientAddressResponseModel
                {
                    FirstLine = a.FirstLine,
                    SecondLine = a.SecondLine,
                    City = a.City,
                    Country = a.Country,
                    ZipCode = a.ZipCode,
                    Details = a.Details,
                }).ToList()
            }).ToList();
            var paged = new PagedList<GetPatientResponseModel>(patientResponse, patients.Count, patients.CurrentPage, patients.PageSize);

            return new PagedResponse<PagedList<GetPatientResponseModel>>(paged, patients.Count, patients.CurrentPage, patients.PageSize);
        }
    }
}
