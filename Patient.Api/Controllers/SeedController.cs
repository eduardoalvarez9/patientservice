﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientService.Database;
using PatientService.Database.Models;
using PatientService.Database.Repositories;

namespace PatientService.Api.Controllers
{
    [Route("api/seed")]
    [Authorize]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly DataSeed _dataSeed;
        public SeedController(DataSeed dataSeed)
        {
            _dataSeed = dataSeed;
        }

        [HttpGet("patient")]
        public async Task<IActionResult> SeedAsync()
        {
            await _dataSeed.SeedPatient();
            return Ok();
        }
    }
}
